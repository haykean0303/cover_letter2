import React, { Component } from 'react';
import Informations from './components/Informations';
import 'bootstrap/dist/css/bootstrap.min.css';

export default class App extends Component {
  render() {
    return (
      <div className="App" id ="main">
        <Informations />
      </div>
    )
  }
}
