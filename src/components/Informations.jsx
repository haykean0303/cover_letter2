import React from 'react';
import profile from "../images/profile.jpg";
import signature from "../images/signature.png";
import { Container, Row, Col, } from "react-bootstrap";
import { AiOutlinePhone, AiOutlineMail, AiOutlineLink } from "react-icons/ai";

function Informations() {
    return (
        <div>
            <Container>
            <Row className="justify-content-md-center">
                <Col style={{color: 'white'}} sm={4} className="bg-dark">
                    <div>
                        <img style={{borderRadius: "50%"}} src={profile} alt="" width="250" height="200" />
                    </div>
                    <div style={{ color: 'white', borderColor: 'white' }}>
                        <h1 style={{border: "2px solid white"}}>CONTACTS</h1>
                        <p>ADDRESS</p>
                    </div>
                    <div className = {"d-flex justify-content-center flex-direction-column"}>
                        <AiOutlinePhone>
                        </AiOutlinePhone>
                        <p>PHONE NUMBER</p>
                    </div>
                    <div className = {"d-flex justify-content-center flex-direction-column"}>
                        <AiOutlineMail>
                        </AiOutlineMail>
                        <p>EMAIL</p>
                    </div>
                    <div className = {"d-flex justify-content-center flex-direction-column"}>
                        <AiOutlineLink>
                        </AiOutlineLink>
                        <p>WEBSITE</p>
                    </div>
                </Col>
                <Col sm={8} className= {"justify-content-start"}>
                    <div>
                        <h1>Gareth Southgate</h1>
                        <h3>Graphic Designer</h3>
                    </div>
                    <div>
                        <h1 style={{border: "2px solid black"}}>COVER LETTER</h1>
                    </div>
                    <div>
                        <h4>Date</h4>
                        <p>20 07 2018</p>
                    </div>
                    <div>
                        <h4>TO<br/>NAME</h4>
                        <p>POSITION</p>
                        <p>COMPANY NAME</p>
                        <p>COMPANY ADDRESS</p>
                    </div>
                    <div>
                        <h4>COMPANY OWNER NAME,</h4>
                        <p style={{textAlign: 'justify',color:'#1B3D6C', margin: 20}}>A cover letter, also known as an application letter, is a personalized letter from you to the person overseeing the hiring process for the job you’re applying for.

A cover letter is not the same as a résumé. While a résumé provides a clear, point-by-point map of your career thus far, a cover letter tells the personal side of your career story. Ideally,
 your cover letter and résumé complement each other, with each document answering any questions the recruiter has about your skills and work experience after reading the other</p>
                    </div>
                    <div>
                        <p>Your Signature</p><br/>
                        <img src={signature} alt="" width="250" height="200" />

                    </div>
                </Col>
            </Row>
            </Container>
        </div>
    )
}

export default Informations;
